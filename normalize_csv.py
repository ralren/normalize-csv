from dateutil import parser, tz
import os
import sys
import csv
import datetime

pacific_zone = tz.gettz('US/Pacific')
eastern_zone = tz.gettz('US/Eastern')
hour_seconds = 3600
minute_seconds = 60

def normalize_duration(duration):
    """Gets the number of seconds in a duration."""
    hour, minutes, seconds = duration.split(':')

    return int(hour) * hour_seconds + int(minutes) * minute_seconds + float(seconds)

def normalize_zip_code(zip_code):
    """Pads zeroes to zip code if less than 5 characters long."""
    length = len(zip_code)
    
    if length < 5:
        num_padded = 5 - length
        padded_zeroes = '0' * num_padded
        
        return padded_zeroes + zip_code

    return zip_code

def normalize_address(address):
    """Puts back quotation marks if address has a comma in it."""
    if ',' in address:
        return '"{0}"'.format(address)

    return address

def normalize_timestamp(timestamp):
    """Changes timestamp's zone from Pacific to Eastern."""
    time = parser.parse(timestamp)
    pacific_time = time.replace(tzinfo=pacific_zone)
    eastern_time = pacific_time.astimezone(eastern_zone)

    return eastern_time.strftime('%m/%d/%y %I:%M:%S %p')

def normalize_content(row, row_num):
    """Goes through the content of the csv row and normalize it."""
    try:
        timestamp = normalize_timestamp(row[0])
        address = normalize_address(row[1])
        zip_code = normalize_zip_code(row[2])
        name = row[3].upper()
        foo_duration = normalize_duration(row[4])
        bar_duration = normalize_duration(row[5])
        total_duration = foo_duration + bar_duration
        notes = row[7]

        print('{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}'.format(timestamp, address, zip_code, name, foo_duration, bar_duration, total_duration, notes))
    except ValueError:
        sys.stderr.write("Warning: error parsing through row {0}. Dropping from normalized csv.".format(row_num))
        return []

def normalize_csv():
    """Takes the csv and normalizes it according to the specs given."""
    with open('temp.csv', 'r', encoding='utf-8', errors='replace') as f:
        row_num = 1
        is_header = True

        for row in csv.reader(f):
            # no need to normalize the header so add immediately to list
            if is_header:
                print(', '.join(row))
                is_header = False
            else:
                normalize_content(row, row_num)
            row_num += 1
    
    # no longer need the temporary csv so can delete
    os.remove('temp.csv')

if __name__ == '__main__':
    input_file = sys.stdin

    # create a temporary csv
    with open('temp.csv', 'w', encoding='utf-8') as f:
        for line in input_file:
            f.write(line)
    
    normalize_csv()