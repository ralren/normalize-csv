# Truss Software Engineering Work Sample
By [Ren R Delos Reyes](https://github.com/ralren)

## Installation Instructions 
Assumes the user will run this on a Mac with `homebrew` and `git` installed.
1. Open Terminal.
2. If you don't already have the latest Python 3 (ver 3.6.5 at the time this was written) installed, please run: ```brew install python```.
3. Clone the work sample's repo by running: ```git clone git@gitlab.com:ralren/normalize-csv.git``` or  ```git clone https://gitlab.com/ralren/normalize-csv.git```.
4. Change the directory to the repo: `cd normalize-csv`.
5. Get the libraries required to run this repo with: ```pip3 install -r requirements.txt```.
6. To run the work sample, type the following into the command line, replacing `input.csv` and `output.csv` with your respective files: ```cat input.csv | python3 normalize_csv.py > output.csv```

## Technologies Used
I used the pre-packaged libraries `sys`, `csv`, `os`, `datetime`, and `unittest`. The only external library is `dateutil`. You can find documentation [here](https://dateutil.readthedocs.io/en/stable/). I used this library to parse the timestamp data provided and convert it to the right timezone.

## Requirements
This section will be used to demonstrate that I've met the requirements for this work sample.

This program takes a CSV formatted file on stdin through the command line and will stdout to the CSV it's piping to.

In this file you will find:
* The `Timestamp` column is formatted in ISO-8601 and has been converted from the US/Pacific timezone to te US/Eastern timezone. 
* The `ZIP` column is formatted to have 5 digits with 0's leading if necessary.
* The `FullName` column handles both English and non-English names and converts these names to uppercase if allowed.
* The `Address` column is passed as it is with invalid Unicode characters replaced.
* Both the `FooDuration` and `BarDuration` column are converted into seconds.
* The `TotalDuration` column is the sum of the seconds calculated for the `FooDuration` and `BarDuration` column.
* The notes column is taken as is except for invalid Unicode characters that have been replaced.

If any of these columns are unable to be parsed becaused of a Unicode Replacement Character, the program will print a warning to `stderr` and will not add the row to the new csv.

## Bonuses
I added tests to the work sample. These can be run with: `python test.py`.