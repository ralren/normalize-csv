from normalize_csv import normalize_timestamp, normalize_address, normalize_zip_code, normalize_duration
import unittest

class TestNormalizeCsv(unittest.TestCase):
    def test_normalize_timestamp(self):
        self.assertEqual(normalize_timestamp('4/1/11 11:00:00 AM'), '04/01/11 02:00:00 PM')
        self.assertEqual(normalize_timestamp('4/30/18 11:36:52 PM'), '05/01/18 02:36:52 AM')
        self.assertNotEqual(normalize_timestamp('3/31/94 6:17:33 PM'), '3/31/18 06:17:33 PM')

    def test_normalize_address(self):
        self.assertEqual(normalize_address('123 4th St, Anywhere, AA'), '"123 4th St, Anywhere, AA"')
        self.assertEqual(normalize_address('123 Address St.'), '123 Address St.')

    def test_normalize_zip_code(self):
        self.assertEqual(normalize_zip_code('1'), '00001')
        self.assertEqual(normalize_zip_code('12345'), '12345')

    def test_normalize_duration(self):
        self.assertEqual(normalize_duration('111:23:32.123'), 401012.123)
        self.assertEqual(normalize_duration('00:00:00.00'), 0)

if __name__ == '__main__':
    unittest.main()
